""" This class will create the Real player and computer player needed for 
the Uno game."""

from cards import Deck, Discard, WildCard, BasicCard, DrawTwoCard, SkipCard
import random
import sys

class color:
    """Class for Colored Texts
    Source: https://stackoverflow.com/questions/8924173/
    """
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    DARKCYAN = '\033[36m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'
    
COLORS = ["red", "green", "blue", "yellow"]

class Player:
    """Creating the players for Uno game and computer players.
    
    Attribute:
        name (str): The name of the real player.
        md_2 (list): The master deck for the UNO game.
        disc (list): The discard pile for the UNO game. 
    """
    
    def __init__(self, deck, disc, name="Player1"):
        """Set the players name .
        
        Args:
            name (str): The name of the player, default="Player1"
            deck (Deck class): The deck for the UNO game.
            disc (Discard class): The discard pile for the Uno game.
            last_placed (Card Object): Last placed card in the discard pile
        Raises:
            ValueError: if the player name is only numbers
        """
        if type(name) is int:
            raise ValueError("Please make your name only letters or in quotes")
        self.name = name
        self.md_2 = deck
        self.disc = disc
        
               
    def take_turn(self):
        """Let the player continue the game.
        
        side effect:
            It places the card into the deck.
        """
        raise NotImplementedError
      
        
class ComputerPlayer(Player):
    """Creating the virtual player to play with the real player.
    
    Attribute:
        last_placed (Type of Card class): The last placed card.
        deck (list): The list of cards in the comp player's deck.
        master_deck (list): The master deck for the game.
    """
    
    def set_deck(self):
        """ Sets the deck for the ComputerPlayer to use."""
        self.deck = self.md_2.comp_deck[:]
        for i in self.deck:
            self.deck = i[:]
        self.master_deck = self.md_2.master_deck[:]
    
    def take_turn(self):
        """Allow the computer to place a card.
        
        side effect:
            The computer player place the card into the deck.
        """
        
        while True:
            if hasattr(self.disc.discard[-1], "draw") and \
            self.disc.discard[-1].draw == "+2":
                add_cards = random.choices(self.master_deck, k=2)
                for card in add_cards:
                    self.deck.append(card)
                    self.master_deck.remove(card)
                    self.disc.discard_pile(
                        BasicCard(self.disc.current_color, None))
                
                print(f"{self.name} drew 2 cards!")
                print(f"{self.name} has {len(self.deck)} cards left!\n")
            
            elif isinstance(self.disc.discard[-1], WildCard) and \
            self.disc.discard[-1].color == "+4":
                add_cards = random.choices(self.master_deck, k=4)
                for card in add_cards:
                    self.deck.append(card)
                    self.master_deck.remove(card)
                    self.disc.discard_pile(
                        BasicCard(self.disc.current_color, None))
                    
                print(f"{self.name} drew 4 cards!")
                print(f"{self.name} has {len(self.deck)} cards left!\n")
                    
            choice = [item for item in self.deck if item.color == 
                      self.disc.current_color or isinstance(item, WildCard) or
                      item.number == self.disc.current_num and item.number != 
                      None]
            
            
            if len(choice) == 0:
                drawn = random.choice(self.master_deck)
                self.master_deck.remove(drawn)
                if drawn.color == self.disc.current_color or drawn.number == \
                self.disc.current_num:
                    if isinstance(drawn, SkipCard):
                        self.disc.discard_pile(drawn)
                        self.disc.current_num = None
                        self.disc.current_color = self.disc.discard[-1].color
                        print(f"{self.name} placed {drawn}!")
                        print(f"{self.name} skipped your turn!\n")
                        continue
                    else:
                        self.disc.discard_pile(drawn)
                        self.disc.current_num = self.disc.discard[-1].number
                        self.disc.current_color = self.disc.discard[-1].color
                        print(f"{self.name} drew and placed {drawn}!")
                        print(f"{self.name} has {len(self.deck)} cards left!\n")
                        break
                
                elif drawn.color == "wild" or drawn.color == "+4":
                    temp_colors = COLORS[:]
                    temp_colors.remove(self.disc.current_color)
                    new = random.choice(temp_colors)
                    self.disc.current_color = new
                    self.disc.current_num = None
                    self.disc.discard_pile(drawn)
                    print(f"{self.name} drew and placed {drawn}!")
                    print(f"{self.name} has {len(self.deck)} cards left!\n")
                    print(f"The color is now: {self.disc.current_color}!\n")
                    break
                else:
                    self.deck.append(drawn)
                    print(f"{self.name} drew a card!")
                    print(f"{self.name} has {len(self.deck)} cards left!\n")
                    break
            else:
                comp_choice = random.choice(choice)
                
                if isinstance(comp_choice, SkipCard):
                    self.deck.remove(comp_choice)
                    self.disc.discard_pile(comp_choice)
                    self.disc.current_num = None
                    self.disc.current_color = self.disc.discard[-1].color
                    print(f"{self.name} placed {comp_choice}!")
                    print(f"{self.name} skipped your turn!\n")
                    continue
                elif isinstance(comp_choice, WildCard):
                    new_color = random.choice(COLORS)
                    self.disc.discard_pile(comp_choice)
                    self.deck.remove(comp_choice)
                    self.disc.current_num = None
                    print(f"{self.name} placed {comp_choice}!")
                    print(f"{self.name} has {len(self.deck)} cards left!\n")
                    print(f"The color is now: {new_color}!\n")
                    self.disc.current_color = new_color
                    break
                else: 
                    self.deck.remove(comp_choice)
                    self.disc.discard_pile(comp_choice) 
                    self.disc.current_num = self.disc.discard[-1].number
                    self.disc.current_color = self.disc.discard[-1].color
                    print(f"{self.name} placed {comp_choice}!")
                    print(f"{self.name} has {len(self.deck)} cards left!\n") 
                    break
                    
                            
class RealPlayer(Player):
    """Allows the real player to place the card.
    
    Attribute:
        name (str): The name of the real player.
        md_2 (list): The master deck for the UNO game.
        disc (list): The discard pile for the UNO game. 
        last_placed (Type of Card class): The last placed card.
        deck (list): The list of cards in the player's deck.
        master_deck (list): The master deck for the game.
    """
    
    def set_deck(self):
        """ Sets the deck for the RealPlayer to use."""
        self.deck = self.md_2.real_deck[:]
        for i in self.deck:
            self.deck = i[:]
        self.master_deck = self.md_2.master_deck[:]

    def take_turn(self):
        """Allows the real players to take turn
        
        side effect:
            The real player place the card into the deck or draws a card.
            There is an input statement and also prints messages.
            Exits the game through sys.exit if user states it.
        """
        
        while True:
            
            if hasattr(self.disc.discard[-1], "draw") and \
            self.disc.discard[-1].draw == "+2":
                add_cards = random.choices(self.master_deck, k=2)
                
                for card in add_cards:
                    self.deck.append(card)
                    self.master_deck.remove(card)
                    self.disc.discard_pile(
                        BasicCard(self.disc.current_color, None))
                print(f"{self.name} drew 2 cards!")
                    
            elif isinstance(self.disc.discard[-1], WildCard) and \
            self.disc.discard[-1].color == "+4":
                add_cards = random.choices(self.master_deck, k=4)
                for card in add_cards:
                    self.deck.append(card)
                    self.master_deck.remove(card)

                    self.disc.discard_pile(
                        BasicCard(self.disc.current_color, None))
                print(f"{self.name} drew 4 cards!")
 
            print(f"\nLast placed color is: {self.disc.current_color}\n")
            if isinstance(self.disc.discard[-1], BasicCard) and \
            self.disc.current_num != None:
                print(f"Last placed number is: {self.disc.current_num}\n")
            print(f"Here is your deck: \n{self.deck}\n")

            card_type = input("Choose your card color." 
                              " (Say wild for a Wild Card, or draw to Draw)\n"
                              "Say exit if you want to exit:  ").lower()

            if card_type == "exit":
                sys.exit()
                
            elif card_type == "draw":
                drawn = random.choice(self.master_deck)
                self.master_deck.remove(drawn)
                self.deck.append(drawn)
                print(f"You just picked up {repr(drawn)}\n")
                break
            
            elif card_type == "wild" or card_type== "+4":
                change_color = input("Pick color you would like to change to: ")
                card = WildCard(color = card_type)
                if change_color in COLORS:
                    self.deck.remove(card)
                    self.disc.discard_pile(card)
                    print(f"The color is now: {change_color}\n")
                    self.disc.current_color = change_color
                    self.disc.current_num = None
                    break
                else:
                    print("Not a valid color, try again!")
                    continue
            
            elif card_type in COLORS:
                num = input("Choose your number(0 ~ 9) / Skip / +2: ").lower()
                if num == "skip":
                    card = SkipCard(card_type)
                    self.deck.remove(card)
                    self.disc.discard_pile(card)
                    self.disc.current_num = None
                    print()
                    print(f"{self.name} its still your turn!\n")
                    continue
                if num == "+2":
                    card = DrawTwoCard(card_type)
                    self.deck.remove(card)
                    self.disc.discard_pile(card)
                    self.disc.current_num = None
                    print()
                    break
                card = BasicCard(card_type, int(num))
                if int(num) == self.disc.current_num and card in self.deck:
                    self.deck.remove(card)
                    self.disc.discard_pile(card)
                    self.disc.current_num = self.disc.discard[-1].number
                    self.disc.current_color = self.disc.discard[-1].color
                    print()
                    print(self.deck)
                    break
                elif card in self.deck and card_type == self.disc.current_color:
                    self.deck.remove(card)
                    self.disc.discard_pile(card)
                    self.disc.current_num = self.disc.discard[-1].number
                    self.disc.current_color = self.disc.discard[-1].color
                    print()
                    print(self.deck)
                    break
                else:
                    print("Not in your deck, please try again!")
                    continue
                
                
    
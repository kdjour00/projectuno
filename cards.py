""" This script will create the cards and deck for the game Uno."""

import random

COLORS = ["red", "green", "blue", "yellow"]
NUMBERS = [None,0,1,2,3,4,5,6,7,8,9]


class BasicCard:
    """ Class for creating the basic cards
    
    Attributes:
        color (str): string for the color
        number (int): for the card's number
    """
    def __init__(self, color, number):
        """ Initializes the basic cards for UNO.
        
        Args:
            color (str): The color of the card.
            number (int): The number for the card.
        """
        if color not in COLORS:
            raise ValueError
        if number not in NUMBERS:
            raise ValueError
        
        self.color = color
        self.number = number
    
    def __eq__(self, other):        
        """ Creating equal symbol for BasicCards.
        
        Args:
            other: Instance of BasicCards.
        """
        return (isinstance(other,BasicCard) and self.color == other.color and 
            self.number == other.number)
        
        
    def __repr__(self):
        """ How the card will be display to the user.
        
        Returns:
            The color and number of the BasicCard.
        """
        return f"({str(self.color)}, {int(self.number)})"
    

class WildCard:
    """Class for creating a WildCard (including +4)
    
    Attributes:
        cardtype: string that discerns if it is Wild or a +4.
        change: string that shows any color that changes into.
        number: None
    """
    def __init__(self, color="wild", number=None):
        """ Initializes the WildCard type. 
        
        Raises:
            ValueError: If color isn't wild or +4.
        """
        if color.lower() == "wild" or color == "+4":
            color = color.lower()
        else:
            raise ValueError
        self.change = None
        self.color = color
        self.number = None
    
    def __eq__(self, other):
        """ Creating equal symbol for WildCard.
        
        Args:
            other: Instance of WildCard.
        """
        return (isinstance(other,WildCard) and self.color == other.color and 
            self.number == other.number)
            
    def __repr__(self):
        """ How the card will display to the user.
        
        Returns:
            The color of the wildcard, which will be "wild"
        """
        return f"{self.color}"
    
    
class SkipCard:
    """Class for creating a skip card
    
    Attributes:
        color: string for color of the card
        skip: string for skip method.
        Number: none
    """
    
    def __init__(self,color,skip="skip", number=None):
        """ Initializes the SkipCard.
        
        Raises:
            ValueError: If color not in colors. Or if skip attribute not skip.
        """
        if color not in COLORS:
            raise ValueError
        if skip.lower() != "skip":
            raise ValueError
        self.color = color.lower()
        self.skip = skip.lower()
        self.number = None
    
    def __eq__(self, other):
        """ Creating equal symbol for SkipCard.
        
        Args:
            other: Instance of SkipCard.
        """
        return (isinstance(other,SkipCard) and self.color == other.color and 
            self.skip == other.skip)
    
    def __repr__(self):
        """ How the card will display to the user.
        
        Returns:
            The color and skip parts of the SkipCard.
        """
        return f"({str(self.color)}, {str(self.skip)})"
        
        
class DrawTwoCard:
    """Class for creating a +2 card
    
    Attributes:
        color (color list): string for color of the card
    """
    
    def __init__(self,color,draw="+2",number=None):
        """ Initializes the Draw Two card method. 
        
        Raises:
            ValueError: If color is not in COLORS list.
        """
        if color not in COLORS:
            raise ValueError
        self.color = color.lower()
        self.draw = draw
        self.number = None

    def __eq__(self, other):
        """ Creating equal symbol for DrawTwoCard.
        
        Args:
            other: Instance of DrawTwoCard.
        """
        return (isinstance(other,DrawTwoCard) and self.color == other.color and 
            self.draw == other.draw)
        
    def __repr__(self):
        """ How the card will display to the user.
        
        Returns:
            The color and skip parts of the DrawTwoCard.
        """
        return f"({str(self.color)}, {str(self.draw)})"

class Deck:
    """Creates a full deck of UNO cards.
    
    Attributes:
        master_deck (list): list of USABLE UNO cards
        comp_deck (list): The Computer's deck.
        real_deck (list): The real player's deck. 
    """
    
    def __init__(self):
        """ Initializes all the types of cards that will be used for the UNO
        game.
        """
        self.master_deck = []
        self.comp_deck = []
        self.real_deck = []
        colors = ["red", "blue", "green", "yellow"]
        
        #Basic Zero
        for c in colors:
            self.master_deck.append(BasicCard(c,0))
        
        #Basic Numbers 1-9
        for n in range(0,2):
            for num in range(1,10):
                for c in colors:
                    self.master_deck.append(BasicCard(c,num)) 
                
        #Wild (Reg & +4)
        for n in range(0,5):
            self.master_deck.append(WildCard("wild"))
            
        for n in range(0,5):
            self.master_deck.append(WildCard("+4"))
        
        #Skip
        for n in range(0,2):
            for c in colors:
                self.master_deck.append(SkipCard(c))
            
        #DrawTwo
        for n in range(0,2):
            for c in colors:
                self.master_deck.append(DrawTwoCard(c))
                
        #CompDeck        
        self.comp_deck.append(random.choices(self.master_deck, k=7))
        for card in self.comp_deck:
            if card in self.master_deck:
                self.master_deck.remove(card)
        
        #return self.master_deck
    
    def player_deck(self):
        """ Creates a player deck for both players also removes cards from the 
        master deck.
        """
        
        self.real_deck.append(random.choices(self.master_deck, k=7))
        for card in self.real_deck:
            if card in self.master_deck:
                self.master_deck.remove(card)
       
        # self.comp_deck.append(random.choices(self.master_deck, k=7))
        # for card in self.comp_deck:
        #     if card in self.master_deck:
        #         self.master_deck.remove(card) 
        #return self.real_deck

class Discard:
    """Discard pile. Starts with one random card at beginning of game
    
    Attributes:
        discard (list): A list for the discarded cards.
        current_color (str): A string that holds the last color placed
        current_num (str): A string that holdds the last number placed
    """
    
    def __init__(self, master):
        """Creates a new discard pile. """
        self.discard = []
        self.md = master.master_deck
        
        #Takes a random card from the master deck and uses it as discard
        first = random.choice(self.md)
        
        while True:
            if isinstance(first, BasicCard):
                self.discard.append(first)
                break
            else:
                first = random.choice(self.md)
                continue
            
        #Removes the first discarded card from master deck
        self.md.remove(self.discard[0])
        self.current_color = self.discard[-1].color
        self.current_num = self.discard[-1].number

    
    def discard_pile(self, card):
        """Discards a card into the pile
        
        Attributes:
            card (BasicCard, WildCard, SkipCard, DrawTwoCard): a card object 
                that is being discarded
        """
        self.discard.append(card)
"""Unit Tests for the Uno Game"""

import unittest

from players import Player
from players import ComputerPlayer
from players import RealPlayer

from cards import BasicCard, WildCard, SkipCard, DrawTwoCard, Deck, Discard

from uno_game import Uno

class TestPlayer(unittest.TestCase):
    """Class to test the methods and attributes of the Players class
    
    Attributes: 
        comp_player = first test player
        real_player = second test player
    """
    
    def setUp(self):
        """Creating real and AI players"""
        self.game_deck = Deck()
        self.game_deck.player_deck()
        self.game_discard = Discard(self.game_deck)
        self.comp_player = ComputerPlayer(self.game_deck, self.game_discard, 
                                          "AI")
        self.real_player = RealPlayer(self.game_deck, self.game_discard, "Real")
        for i in self.comp_player.md_2.comp_deck:
            self.comp_deck = i[:]
        for i in self.real_player.md_2.real_deck:
            self.real_deck = i[:]
        
    def test_init(self):
        """ Testing the init method for the Players class. """
        self.assertEqual(self.comp_player.name, "AI")
        self.assertEqual(self.real_player.name, "Real")
        with self.assertRaises(ValueError):
            RealPlayer(self.game_deck, self.game_discard, 323)
        with self.assertRaises(ValueError):
            ComputerPlayer(self.game_deck, self.game_discard, 323)
            
    def test_set_deck(self):
        """ Does the set_deck method work for the RealPlayer and ComputerPlayer
        class?
        """
        self.assertEqual(len(self.comp_player.md_2.master_deck), 101)
        self.assertEqual(len(self.comp_deck), 7)
        self.assertIsInstance(self.comp_deck, list)
        self.assertEqual(len(self.real_player.md_2.master_deck), 101)
        self.assertEqual(len(self.real_deck), 7)
        self.assertIsInstance(self.real_deck, list)
        
        
    def test_take_turn(self):
        """Will be manually tested"""
        pass
    
    
class TestBasicCard(unittest.TestCase):
    """Class to test the BasicCard types based on their __repr__
    
    Attributes:
        bcard1,
        bcard2,
        bcard3: Expected output for basic Card.
    """
    
    def setUp(self):
        self.bcard1 = '(red, 9)'
        self.bcard2 = '(blue, 7)'
        self.bcard3 = '(yellow 9)'
        
    def test_basic(self):
        """ Does the __repr__ method work for the BasicCard class? """
        self.assertEqual(self.bcard1, str(BasicCard("red", 9)))
        self.assertEqual(self.bcard2, str(BasicCard("blue", 7)))
        self.assertNotEqual(self.bcard3, str(BasicCard("yellow", 9)))
        with self.assertRaises(ValueError):
            str(BasicCard("purple", 4))
        with self.assertRaises(ValueError):
            str(BasicCard("red", 11))    
            
class TestWildCard(unittest.TestCase):
    """Class to test the WildCard types based on their __repr__
    
    Attributes:
        wcard1,
        wcard2: Expected output for Wild Card.
    """
    
    def setUp(self):
        self.wcard1 = ("wild")
        self.wcard2 = ("+4")
        
    def test_wild(self):
        """ Does the __repr__ method work for the WildCard class? """
        self.assertEqual(self.wcard1, str(WildCard("wild")))
        self.assertEqual(self.wcard2, str(WildCard("+4")))
        with self.assertRaises(ValueError):
            str((WildCard("fake")))

class TestSkipCard(unittest.TestCase):
    """Class to test the SkipCard types based on their __repr__
    
    Attributes:
        scard1,
        scard2,
        scard3: Expected output for Skip Card.
    """
    
    def setUp(self):
        self.scard1 = '(red, skip)'
        self.scard2 = '(blue, skip)'
        self.scard3 = '(yellow, skip)'
        
    def test_skip(self):
        """ Does the __repr__ method work for the SkipCard class? """
        self.assertEqual(self.scard1, repr(SkipCard("red")))
        self.assertEqual(self.scard2, repr(SkipCard("blue")))
        with self.assertRaises(ValueError):
            self.assertEqual(self.scard3, repr(WildCard("red")))
        with self.assertRaises(ValueError):
            repr((SkipCard("yellow", "123")))  
        with self.assertRaises(ValueError):
            repr((SkipCard("puple", "skip")))

class TestDrawTwoCard(unittest.TestCase):
    """Class to test the DrawTwoCard types based on their __repr__
    
    Attributes:
        dcard1,
        dcard2,
        dcard3: Expected output for Draw Two Card.
    """
    
    def setUp(self):
        self.dcard1 = '(red, +2)'
        self.dcard2 = '(blue, +2)'
        self.dcard3 = '(yellow, +2)'
        
    def test_draw_card(self):
        """ Does the __repr__ method work for the DrawTwoCard class? """
        self.assertEqual(self.dcard1, str(DrawTwoCard("red", "+2")))
        self.assertEqual(self.dcard2, str(DrawTwoCard("blue")))
        self.assertNotEqual(self.dcard3, str(DrawTwoCard("green")))
        with self.assertRaises(ValueError):
            str((SkipCard("chrome")))                  

class TestDeck(unittest.TestCase):
    """Class to test the Deck class. """
    
    def test_init(self):
        """ Does the __init__ method work for the Deck class?"""
        self.assertIn(BasicCard("red", 4), Deck().master_deck)
        self.assertIn(BasicCard("blue", 7), Deck().master_deck)
        self.assertIn(BasicCard("yellow", 3), Deck().master_deck)
        with self.assertRaises(ValueError):
            BasicCard("pink", 7)
        with self.assertRaises(ValueError):
            BasicCard("blue", 17)
        with self.assertRaises(ValueError):
            WildCard("+2")
        self.assertIn(WildCard("wild"), Deck().master_deck)
        self.assertIn(WildCard("+4"), Deck().master_deck)
        self.assertIn(SkipCard("red"), Deck().master_deck)
        self.assertIn(SkipCard("blue"), Deck().master_deck)
        self.assertIn(SkipCard("yellow"), Deck().master_deck)
        self.assertIn(SkipCard("green"), Deck().master_deck)
        self.assertIn(DrawTwoCard("red"), Deck().master_deck)
        self.assertIn(DrawTwoCard("blue"), Deck().master_deck)
        self.assertIn(DrawTwoCard("yellow"), Deck().master_deck)
        self.assertIn(DrawTwoCard("green"), Deck().master_deck)
        with self.assertRaises(ValueError):
            DrawTwoCard("pink")
    
    def test_player_deck(self):
        """ Does the player_deck method work for the Deck class?"""
        game_deck = Deck()
        game_deck.player_deck()
        for item in game_deck.real_deck:
            self.real = item[:]
        for item1 in game_deck.comp_deck:
            self.bot = item1[:]
        self.assertEqual(len(self.real), 7)
        self.assertEqual(len(self.bot), 7)
      
class TestUno(unittest.TestCase):
    """Class to test Uno methods and attributes"""
    
    def setUp(self):
        self.game_deck = Deck()
        self.game_deck.player_deck()
        self.game_discard = Discard(self.game_deck)
        self.joe = RealPlayer(self.game_deck, self.game_discard, "Joe")
        self.ai = ComputerPlayer(self.game_deck, self.game_discard, "AI")
        self.game = Uno(self.joe, self.ai)
    
    def test_init(self):
        """ Testing the init method for the Uno game."""
        self.assertEqual("Joe", self.joe.name)
        self.assertEqual("AI", self.ai.name)
        with self.assertRaises(TypeError):
            Uno("Joe", "Bot")
        
    def test_game_loop(self):
        """Will test manually"""
        pass
        
    def test_check_uno(self):
        """Will test manually."""
        pass
        

if __name__ == "__main__":
    unittest.main()
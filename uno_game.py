""" This script will play the game Uno."""

import random
import sys
from cards import Deck, Discard, WildCard, BasicCard, DrawTwoCard, SkipCard

from players import RealPlayer, ComputerPlayer, color

class Uno:
    """ This class allows for the user to start and play the game Uno.
    
    Attributes:
        real (RealPlayer): The real player for the game.
        comp (ComputerPlayer): The computer player of the game.
    """

    
    def __init__(self, player1, player2):
        """Initializes the Uno game with two different players.
        
        Args:
            player1 (RealPlayer class): a player from the RealPlayer class.
            player2 (ComputerPlayer class): a player from the ComputerPlayer 
                class.

        Raises:
            TypeError: If the player1 isn't an instance of a RealPlayer class
                or if player2 isn't an instance of a ComputerPlayer class.
        """
        if isinstance(player1,RealPlayer) == False:
            raise TypeError
        if isinstance(player2,ComputerPlayer) == False:
            raise TypeError
        self.real = player1
        self.comp = player2
        
    
    def game_loop(self):
        """ This will be the game loop where the game will continue in a loop 
        until one of the players is out of cards, ending the game.        
        """
        while True:
            print(f"{self.real.name}, it is your turn!\n")
            self.real.take_turn()
            print("∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎" 
                "∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎"
                "∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎")
            
            if len(self.comp.deck) == 1:
                check = self.check_uno(self.comp)
                if check == False:
                    pass
                else:
                    print(f"{self.comp.name} placed the last card!")
                    print(color.UNDERLINE + f"{self.comp.name} won the game!" \
                          + color.END)
                    break

                
            print(f"{self.comp.name}, it is your turn!")
            self.comp.take_turn()
            print("∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎" 
                "∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎"
                "∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎")

            if len(self.real.deck) == 1:
                check = self.check_uno(self.real)
                if check == False:
                    pass
                else:
                    print(f"{self.real.name} placed the last card!")
                    print(color.UNDERLINE + f"{self.real.name} won uno game!" \
                        + color.END)
                    break
                
    
    def check_uno(self, player_type):
        """ This method will check to see if the user has a uno and they have
        said Uno before placing the card down. If not they will be forced to 
        draw a card.
        
        Args:
            player_type(Player Class): The type of player class.
            
        Returns:
            Boolean: Depending on if they said: uno.
        """
        if type(player_type) is RealPlayer:
            for item in player_type.deck:
                if item.color == player_type.disc.current_color or \
                isinstance(item, WildCard):
                    say = input("say something: ").lower()
                    if say == "uno":
                        return True
                    else:
                        drawn = random.choice(Deck().master_deck)
                        self.real.deck.append(drawn)
                        return False
                else:
                    return False
            
        if type(player_type) is ComputerPlayer:
            for item in player_type.deck:
                if item.color == player_type.disc.current_color or \
                isinstance(item, WildCard):
                    print("Uno!")
                    return True
                else:
                    return False

def main():
    """ This will combine all of the methods of UNO together and apply it
    it so the game is playable.
    """
    print("∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎" 
            "∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎"
            "∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎")
    
    print(color.RED + "≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛"
          "≛≛≛≛≛≛≛≛≛≛≛≛≛≛ " + color.END + "Welcome to " + color.YELLOW + 
          color.BOLD + "UNO!" + color.END +
           color.END + color.RED + "≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛"
           "≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛≛" + color.END)
    
    print("∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎" 
            "∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎"
            "∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎")
    #Creates the game deck and fills players hands
    game_deck = Deck()
    game_deck.player_deck()
    
    #Creates the discard pile
    game_discard = Discard(game_deck)
    
    #Creates the players and takes their name attributes
    real_name =  input("Please enter your name: \n")
    real_name = color.RED + real_name + color.END 
    real_player = RealPlayer(game_deck, game_discard,real_name)
    comp_name = input("Please name your AI opponent: \n")
    comp_name = color.BLUE+ comp_name + color.END 
    comp_player = ComputerPlayer(game_deck, game_discard,comp_name)
    print("∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎" 
            "∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎"
            "∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎")



    main_game = Uno(real_player, comp_player)
    real_player.set_deck()
    comp_player.set_deck()
    main_game.game_loop()
    
if __name__ == "__main__":
    main()